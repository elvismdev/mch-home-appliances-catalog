<?php
define( 'WP_MEMORY_LIMIT', '64M' );
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mchcatalog');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'lR||vseK@CpL>D8Eyz06=YKZxam^5<o#1rC<zrD-CxcYAt%}=hgP|bj[]90txbF3');
define('SECURE_AUTH_KEY',  '2j+Y7Q]I|H7[/]ydwb[VA|orw~HpjP+,.gDz7^_B4-`(BNAd:=/TX=RF 8,eB+Ds');
define('LOGGED_IN_KEY',    '5cM|zAs]XAM489eXP5rlBW(v?:x$|>_$?bHLu>ILz|7:XSI?Bpka`T#XJ%z.yXg+');
define('NONCE_KEY',        'Q75:9Y8KGZcf^wDy+t|6hlpuyubyVf{I7w~L|w/D3!WL?{h*%oF=_6o:B=@cfY80');
define('AUTH_SALT',        '?QV@)`Sv0[<YZgxGVV7.||hrSG22A%!tu_yz72IP68@+^%s-UwZxuij=s/[j`VwT');
define('SECURE_AUTH_SALT', '?|X_)e4Ys7L`$KftN%Roe*<U64pZ}L=ICd)Jq7`U G<hT~{q?j:F1(3-poT(5M?(');
define('LOGGED_IN_SALT',   'U;_+-cZ|kZF28`pTjGiwCMLi{U,Q#QfFmt^>i51ZxfV|?h!)D13+9(|;A2co0t(M');
define('NONCE_SALT',       'n}i~1PIl/+l0xy,Nl57H<{P6FzY<+KxJ<g0=|gpVA0-1Y` `GcpEPs(8S@xJ|+P-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define( 'FS_METHOD', 'direct' );
define( 'FS_CHMOD_DIR', 0777 );
define( 'FS_CHMOD_FILE', 0777 );
